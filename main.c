#include <stdio.h>
#include <stdlib.h>

int initialisedGlobal[4] = {1, 2, 3, 4}; //va dans le .data
int CONSTinitialisedGlobal[4] = {1, 2, 3, 4}; // Va dans le .rodata

int main(){

	printf("Non initialised hors main : %x\n",&initialisedGlobal);
	printf("Non initialised hors main : %x\n",&CONSTinitialisedGlobal);
	
	printf("Coucou\n");
		
	int non_initialised[1024]; //Va dans le .bss
	printf("Non initialised dans main : %x\n",&non_initialised);
	int initialised[4] = {1, 2, 3, 4};
	printf("Initialised dans main : %x\n",&initialised);
	
	int * ptr = (int *) malloc (sizeof(int *));
	printf("pointeur dans main : %x\n",&ptr);
		
	int x = 1;
	int y = 2;
	int z;
	z = x + y;
	
	FILE* fichier = NULL;
	fichier = fopen("test.txt", "w");
	if (fichier != NULL){
		fputs("Hellow", fichier);
		printf("Descripteur de fichier dans main : %x\n",&fichier);
		fclose(fichier);
	}

	for(;;){}
	return 0;
}
