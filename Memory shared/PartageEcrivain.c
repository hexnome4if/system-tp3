#include<stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include<string.h>
int main()
{
	
	int in,i ;

	key_t key = 6000;
	
	//Obtaining Access to shared memory = Récupère identifiateur (int) à partir de la clé
	int shmid = shmget(key, 27, IPC_CREAT | 0666);
	
	//Gestion des problèmes
	if(shmid<0)
	{
		perror("Writer Error: Access Problem");
		return 0;
	}
 
	//Attaching the shared memory = Récupère un pointeur vers la zone mémoire
	 char *shm = shmat(shmid, NULL, 0); 
      
      	
	//Gestion des problèmes
	if(shm=="-1")
	{
		perror("Writer Error: Problem in attaching");
	        return 0;
	}

	//Inserting str into the shared memory..!!
	
	char str[100] = "INPUTABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char *s = shm;
	printf("\nWriter Wrote: ");
		
	for (i=0;i<100;i++)
	{
		char c = str[i];	
		putchar(c);
		//printf("Ecrit : %d\n", i);
		*s++ = i;
    }
	*s = '\0';
	
	printf("\nWRITER SLEEPING\n");
	while (*shm!= '*')
        	sleep(1);
	
	return 0;

}
