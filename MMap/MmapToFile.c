#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

int main(int argc, const char *argv[])
{   
	// Paths of source and destination files
    //const char *filepath = "tp3.c";
	//const char *filepathDest = "tp3DEST2.c";
	
	// Opening source file (read) and destination file (write) 
    int fd = open(argv[1], O_RDONLY, (mode_t)0600);
    int dest = open(argv[2], O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);

	// Checking openings are OK
    if (fd == -1)
    {
        perror("Error opening source file for writing");
        exit(EXIT_FAILURE);
    }        
	if (dest == -1)
    {
        perror("Error opening destination file for writing");
        exit(EXIT_FAILURE);
    }      

	// GETTING SOURCE FILE SIZE
    struct stat fileInfo = {0};
    if (fstat(fd, &fileInfo) == -1)
    {
        perror("Error getting the file size");
        exit(EXIT_FAILURE);
    }
    if (fileInfo.st_size == 0)
    {
        fprintf(stderr, "Error: File is empty, nothing to do\n");
        exit(EXIT_FAILURE);
    }
    //printf("File size is %ji\n", (intmax_t)fileInfo.st_size);

	// PROT_READ : mode lecture
	// MAP_SHARED : partagé entre les processus
	// fd : fichier
	// MAPPING SOURCE FILE (read)
    char *map = mmap(0, fileInfo.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED)
    {
        close(fd);
        perror("Error mmapping the source file");
        exit(EXIT_FAILURE);
    }
    // Setting size of destination file (size of source file)
    // If not, we get "Erreur de bus (core dumped)"
	ftruncate(dest, fileInfo.st_size);
	
    // MAPPING DESTINATION FILE (write)
	char *mapDest = mmap(0, fileInfo.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, dest, 0);
    if (mapDest == MAP_FAILED)
    {
        close(fd);
        perror("Error mmapping the destination file");
        exit(EXIT_FAILURE);
    }

    for (off_t i = 0; i < fileInfo.st_size; i++)
    {
        //printf("Found character %c at %ji\n", map[i], (intmax_t)i);
        // Copying character from source to destination
		mapDest[i] = map[i];
    }
    
    // Don't forget to free the mmapped memory
    if (munmap(map, fileInfo.st_size) == -1)
    {
        close(fd);
        perror("Error un-mmapping the source file");
        exit(EXIT_FAILURE);
    }
	 if (munmap(mapDest, fileInfo.st_size) == -1)
    {
        close(dest);
        perror("Error un-mmapping the destination file");
        exit(EXIT_FAILURE);
    }

    // Un-mmaping doesn't close the file, so we still need to do that.
    close(fd);
	close(dest);
    
    return 0;
}
