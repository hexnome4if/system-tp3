#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main (int argc, char *argv[])
{
        struct stat sb; //Structure pour avoir des informations sur le fichier
        off_t len;
        char *p;
        int file;

        if (argc < 2) { //En cas de mauvais nb de param.
                fprintf (stderr, "usage: %s <file>\n", argv[0]);
                return 1;
        }

        file = open (argv[1], O_RDONLY); //Ouverture du fichier en readonly
        
        if (file == -1) {
                perror ("open");
                return 1;
        }

        if (fstat (file, &sb) == -1) {
                perror ("fstat");
                return 1;
        }

        if (!S_ISREG (sb.st_mode)) {
                fprintf (stderr, "%s is not a file\n", argv[1]);
                return 1;
        }

	//loader le fichier mmap avec les stats du fichier, en read only et en partage avec d'autres processeur
        p = mmap (0, sb.st_size, PROT_READ, MAP_SHARED, file, 0);
        
        if (p == MAP_FAILED) {
                perror ("mmap");
                return 1;
        }

        if (close (file) == -1) {
                perror ("close");
                return 1;
        }

        for (len = 0; len < sb.st_size; len++)
        {
                putchar (p[len]); // affichage de caractère
	}
	
        if (munmap (p, sb.st_size) == -1) { //Equivalent à "free", il a tout compris.
                perror ("munmap");
                return 1;
        }

        return 0;
}