#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main (int argc, char *argv[])
{
        struct stat sb; //Structure pour avoir des informations sur le fichier
        off_t len;
        char *p;
        char a;
        FILE* inft;
        FILE* outft;
        int fileread;
        
        if (argc < 3) { //En cas de mauvais nb de param.
                fprintf (stderr, "usage: %s <file> <file>\n", argv[0]);
                return 1;
        }

	/* en cas d'utilisation de buffer 
       char buf[160];
       inft = open (argv[1], O_RDONLY); //Ouverture du fichier en readonly
       outft = open (argv[2], O_CREAT | O_APPEND | O_RDWR); //Ouverture du fichier d'écriture en read/write 
       */
	inft = fopen(argv[1], "r");
	outft =  fopen(argv[2], "w");
	
	do {
	      a = fgetc(inft);
	      if(a!=EOF){
	      	      fputc(a, outft);
	      }
   	} while (a != EOF);
   
	/*
	if(inft >0){ // there are things to read from the input
	    fileread = read(inft, buf, 160);
	    printf("%s\n", buf);
	    write(outft, buf, 160);

	  } 
	  */
	  fclose(inft);
	  fclose(outft);
// Voir : http://www.c4learn.com/c-programs/write-a-program-to-copy-the-contents-of-one-file-into-another-using-fputc.html
        return 0;
}